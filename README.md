# Formations

## Ecoles

- Institut Sainte Anne Gosselies (ISA) : Secondaire 3 TT Info 
- IFAPME Charleroi: Dévelopeurs Web 
- EICVN Namur : Bachelier en informatique : administration des systèmes
- IPES Seraing


### ISA

[ISA](https://www.isagosselies.com/) : Secondaire 3 Techinque Transition Infomatique, composé de 4 Unité d'Acquis d'Apprentissage (UAA)
1) UAA0 Numériques
2) UAA1 Hardware
3) UAA2 Programmation (impérative et procdurale)
4) UAA3 Web (HTML & CSS)

### IFAPME 

- OMB : Outils et méthodes de base.
- DTA : Les données tabulaires
- LDB : Le langage des bases de données (SQL)

### EICVN

- Gérer un projet IT (infrastruture)
- Intitiation à la programmation
- Gestion de parc informatique sous logiciels prpriétaires (Windows).

### IPES

- Intiation à l'informatique

## Principales Sources

Wikipédia (Fr/En)
FUN Mooc
Class Code
...


## Authors and acknowledgment
> Alain Caris 

## License
CC-ZERO pour mes propres contributions

